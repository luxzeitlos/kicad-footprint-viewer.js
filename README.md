![pipeline](https://gitlab.com/xengi/kicad-footprint-viewer.js/badges/master/pipeline.svg)
![pipeline](https://img.shields.io/gitlab/pipeline/xengi/kicad-footprint-viewer.js.svg?style=flat-square)
![coverage](https://gitlab.com/xengi/kicad-footprint-viewer.js/badges/master/coverage.svg)
![coverage](https://img.shields.io/codecov/c/gitlab/xengi/kicad-footprint-viewer.git.svg?style=flat-square)
![version](https://img.shields.io/npm/v/kicad-footprint-viewer.svg?style=flat-square)
![downloads](https://img.shields.io/npm/dt/kicad-footprint-viewer.svg?style=flat-square)
![size](https://img.shields.io/bundlephobia/min/kicad-footprint-viewer.svg?style=flat-square)
![MIT license](https://img.shields.io/npm/l/kicad-footprint-viewer.svg?style=flat-square)

# kicad-viewer

kicad-viewer is a footprint viewer for [KiCAD](http://kicad-pcb.org/) footprint files.

## Usage

Here's a simple snippet that shows how to use kicad-viewer:

```html
<canvas id="kicad" data-footprint="/Teensy3.x_LC.kicad_mod" width="480" height="320"></canvas>

<script src="kicad.js"></script>
<script>
    'use strict';

    var options = {
        grid: 1.27
    };

    var canvas = document.getElementById("#kicad");

    var kicadviewer = new kicad_viewer(canvas, options);

    fetch(canvas.data("footprint"))
        .then(res => res.text())
        .then(data => kicadviewer.render(data));
</script>
```
